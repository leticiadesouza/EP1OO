#include <iostream> // cout, cerr
#include <fstream> // ifstream
#include <sstream> // stringstream
#include <string>
using namespace std;

int main() {
  int row = 0, col = 0, numrows = 0, numcols = 0;
  ifstream infile("/home/leticia/Documentos/EP1OO/obj/lena.pgm");
  stringstream ss;
  string inputLine = "";

  // First line : version
  getline(infile,inputLine);
  if(inputLine.compare("P5") != 0) cerr << "Version error" << endl;
  else cout << "Version : " << inputLine << endl;

  // Second line : comment
  getline(infile,inputLine);
  cout << "Comment : " << inputLine << endl;

  // Continue with a stringstream
  ss << infile.rdbuf();
  // Third line : size
  ss >> numcols >> numrows;
  cout << numcols << " colunas e " << numrows << " linhas" << endl;

  int array[numrows][numcols];

  // Following lines : data
  for(row = 0; row < numrows; ++row)
    for (col = 0; col < numcols; ++col) ss >> array[row][col];

    //Buscando linha e coluna específicas
    cout << "LINHA 511 COLUNA 50: " << array[511][500] << "\n";
  // Now print the array to see the result
  for(row = 0; row < numrows; ++row) {
    cout << "NOVA LINHA "<< row << "************************************************\n";
    for(col = 0; col < numcols; ++col) {
      cout << array[row][col] << " | ";
    }
    cout << endl;
  }
  infile.close();
}